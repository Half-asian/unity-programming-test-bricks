﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class LevelEvents : MonoBehaviour
{
    public static LevelEvents current; //This class acts as a singleton. It enables easy data communication between all the game objects without direct reference.
    public event Action OnLevelWinEvent = delegate { };
    public event Action OnGameWinEvent = delegate { };
    public event Action OnGameLoseEvent = delegate { };
    public event Action<Brick> OnBrickDestroyedEvent = delegate { };
    private void Awake()
    {
        current = this;
    }

    public void onLevelWin()
    {
        OnLevelWinEvent.Invoke();
    }

    public void onGameWin()
    {
        OnGameWinEvent.Invoke();
    }

    public void onGameLose()
    {
        OnGameLoseEvent.Invoke();
    }

    public void onBrickDestroyed(Brick b)
    {
        OnBrickDestroyedEvent.Invoke(b);
    }
}
