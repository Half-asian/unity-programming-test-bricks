﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class LevelController : MonoBehaviour
{
    [SerializeField]
    private List<string> level_playlist; //e.g. Level1, Level2...

    ILevel active_level;

    private int level_counter;

    public void startGame() //Restarts the game from first level in the level playlist
    {
        level_counter = 0;
        startNextLevel();
    }

    public void startNextLevel() //If there is an available level, instantiate the level and spawn the play field. Otherwise, the game is finished.
    {
        if (level_counter < level_playlist.Count)
        {
            if (active_level != null)
                active_level.EndLevel();

            string current_level_name = level_playlist[level_counter];
            
            var level_resource = Resources.Load(current_level_name);
            if (level_resource == null) throw new System.Exception("Level " + current_level_name + " prefab does not exist."); //There needs to be a level prefab of the level name
            GameObject level_gameobject = Instantiate(level_resource) as GameObject; //Spawn the level as a gameobject.
            active_level = level_gameobject.GetComponent<ILevel>();
            active_level.StartLevel();
            level_counter++;
        }
        else
        {
            LevelEvents.current.onGameWin(); //No more levels. Win.
        }
    }
}
