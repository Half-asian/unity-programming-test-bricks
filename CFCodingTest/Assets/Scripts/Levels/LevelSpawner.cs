﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

class LevelSpawner : MonoBehaviour, ILevelSpawner
{
    public Brick SpawnBrick(BrickSpawner brick_spawner) //Spawn a singular brick from a brickspawner. Usually for runtime, to dynamically change the level.
    {
        GameObject new_brick_gameobject = Instantiate(Resources.Load(brick_spawner._brick_resource)) as GameObject;
        new_brick_gameobject.transform.position = brick_spawner._location;
        new_brick_gameobject.transform.parent = transform;
        Brick new_brick = new_brick_gameobject.GetComponent<Brick>();

        return new_brick;
    }

    public void DestroyBricks(List<Brick> active_bricks) //Clear the field, usually if level won or lost.
    {
        foreach (Brick b in active_bricks)
        {
            if (b != null)
                Destroy(b.gameObject);
        }
    }
}

