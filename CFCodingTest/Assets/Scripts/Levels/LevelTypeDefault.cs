﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class LevelTypeDefault : MonoBehaviour, ILevel //Very basic level type. Bricks are pre-spawned in a prefab. Destroy all destructible bricks to complete this level.
{
    private ILevelSpawner _levelSpawner;
    private List<Brick> _activeBricks;
    private int _goalBricksCounter;

    void Awake()
    {
        _levelSpawner = GetComponent<LevelSpawner>();
        LevelEvents.current.OnBrickDestroyedEvent += onBrickDestroyed; //Subscribe to event. Listen to all destroyed bricks.
    }

    private void OnDestroy()
    {
        LevelEvents.current.OnBrickDestroyedEvent -= onBrickDestroyed; //Unsubscribe from event
    }

    public void StartLevel()
    {
        _activeBricks = new List<Brick>(GetComponentsInChildren<Brick>());
        countGoalBricks();
    }

    private void countGoalBricks() //All bricks that aren't indestructible are goal bricks. A user must destroy all to complete the level.
    {
        _goalBricksCounter = 0;
        foreach(Brick brick in _activeBricks)
        {
            if (brick._brickType != "indestructible")
                _goalBricksCounter++;
        }
    }

    public void onBrickDestroyed(Brick b)
    {
        _activeBricks.Remove(b);
        _goalBricksCounter--;
        if (_goalBricksCounter <= 0)
            LevelEvents.current.onLevelWin();
    }

    public void EndLevel()
    {
        _levelSpawner.DestroyBricks(_activeBricks);
        Destroy(gameObject);
    }
}
