﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTypeEndless : MonoBehaviour, ILevel 
{
    //An endless level. Bricks will continuously spawn during the game and move down. 
    //No way to win, but a way to lose if you let the bricks down too far.
    private float _brickSpawnTime = 5.0f;
    private float _spawnCountdown;
    private ILevelSpawner _levelSpawner;
    private List<Brick> _activeBricks;
    private bool _gameStarted = false;
    private bool _triggeredGameLoss = false;
    private string[] brick_types = new string[] { "BrickSingleHit", "BrickDoubleHit", "BrickMoving" };

    void Awake()
    {
        LevelEvents.current.OnBrickDestroyedEvent += onBrickDestroyed;
        _levelSpawner = GetComponent<LevelSpawner>();
        _spawnCountdown = _brickSpawnTime;
    }

    public void StartLevel()
    {
        _activeBricks = new List<Brick>(GetComponentsInChildren<Brick>());
        _gameStarted = true;
    }

    public void EndLevel()
    {
        _levelSpawner.DestroyBricks(_activeBricks);
        Destroy(gameObject);
    }

    public void onBrickDestroyed(Brick b)
    {
        _activeBricks.Remove(b);
    }

    public void Update()
    {
        foreach(Brick brick in _activeBricks)
        {
            if (brick != null)
            {
                brick.transform.Translate(new Vector2(0, -0.1f * Time.deltaTime)); //Move all bricks downwards
                if (brick.transform.position.y < -3.8f && !_triggeredGameLoss) //If any have reached the player, lose the game
                {
                    LevelEvents.current.onGameLose();
                    _triggeredGameLoss = true;
                }
            }
        }

        if (_gameStarted && _spawnCountdown <= 0.0f) //Choose a random brick to spawn ever x seconds
        {
            Vector2 new_location = new Vector2(Random.Range(-3.0f, 3.0f), Random.Range(-1.0f, 2.0f));

            _activeBricks.Add(_levelSpawner.SpawnBrick(new BrickSpawner(brick_types[Random.Range(0, brick_types.Length)], new_location)));
            _spawnCountdown = _brickSpawnTime;
        }
        else
            _spawnCountdown -= Time.deltaTime;
    }

}
