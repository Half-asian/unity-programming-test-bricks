﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public struct BrickSpawner //Simple struct containing data for an ILevelSpawner to spawn a single brick
{
    public string _brick_resource;
    public Vector2 _location;
    public BrickSpawner(string brick_resource, Vector2 location)
    {
        _brick_resource = brick_resource;
        _location = location;
    }
}

interface ILevel //Levels only need to provide these two basic functions. This way, levels can have all sorts of custom behaviour.
{
    void StartLevel();
    void EndLevel();
}

interface ILevelSpawner //Level spawner is responsible for managing the physical level game objects
{
    Brick SpawnBrick(BrickSpawner brick_spawner);
    void DestroyBricks(List<Brick> active_bricks);
}
