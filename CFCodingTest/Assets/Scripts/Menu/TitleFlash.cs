﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TitleFlash : MonoBehaviour
{
    TMPro.TMP_Text _text;
    private void Start()
    {
        _text = GetComponent<TMP_Text>();
    }
    void Update()
    {
        float sin_color = Mathf.Abs(Mathf.Sin(Time.realtimeSinceStartup * 5));
        _text.color = new Color(255, sin_color, sin_color);
    }
}
