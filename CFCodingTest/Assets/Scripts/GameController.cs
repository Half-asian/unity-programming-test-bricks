﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private LevelController _levelController = null;
    [SerializeField] private Ball _ball = null;
    [SerializeField] private Transform _paddleAnchor = null;
    
    [SerializeField] private float _resetBallDelay = 2f;
    [SerializeField] private int _startingLives = 3;
    
    [SerializeField] private GameObject[] _livesCounterIcons = new GameObject[0];
    [SerializeField] private GameObject _gameOverScreen = null;
    [SerializeField] private GameObject _winGameScreen = null;

    private int _lives;
    public int lives
    {
        get => _lives;
        private set
        {
            _lives = value;
            
            // Update lives counter
            for (int i = 0; i < _livesCounterIcons.Length; i++)
            {
                _livesCounterIcons[i].SetActive(i < lives);
            }
        }
    }

    private void Start()
    {
        _ball.BallLostEvent += OnBallLost;
        LevelEvents.current.OnLevelWinEvent += OnLevelWin;
        LevelEvents.current.OnGameWinEvent += OnGameWin;
        LevelEvents.current.OnGameLoseEvent += OnGameLose;
        RestartGame();
    }

    private void OnDestroy()
    {
        _ball.BallLostEvent -= OnBallLost;
    }

    public void RestartGame()
    {
        _gameOverScreen.SetActive(false);
        _winGameScreen.SetActive(false);
        Cursor.visible = false;
        _levelController.startGame();
        _ball.Initialize(_paddleAnchor);
        lives = _startingLives;
    }

    private void Update()
    {
        // Launch ball
        if (_ball != null && _ball.state == Ball.State.Ready)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _ball.Launch();
            }
        }
    }
    
    private void OnBallLost()
    {
        if (lives > 0)
        {
            StartCoroutine(ResetBallProcess());
        }
        else
        {
            StartCoroutine(GameOverProcess());
        }
    }

    public void OnLevelWin()
    {
        StartCoroutine(NextLevelProcess());
    }

    public void OnGameWin()
    {
        Cursor.visible = true;
        _winGameScreen.SetActive(true);
    }

    public void OnGameLose()
    {
        _ball.Initialize(_paddleAnchor);
        _ball.stopBall();
        StartCoroutine(GameOverProcess());
    }

    private IEnumerator NextLevelProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);

        _ball.Initialize(_paddleAnchor);
        _levelController.startNextLevel();
    }

    private IEnumerator ResetBallProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);
        
        _ball.Initialize(_paddleAnchor);
        lives--;
    }

    private IEnumerator GameOverProcess()
    {
        yield return new WaitForSeconds(_resetBallDelay);
        
        _gameOverScreen.SetActive(true);
        Cursor.visible = true;
    }
}
