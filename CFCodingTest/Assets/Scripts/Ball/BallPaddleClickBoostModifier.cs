﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPaddleClickBoostModifier : MonoBehaviour, IBallModifier
{
    [SerializeField]
    private Transform _paddleTransform;
    [SerializeField]
    private float _maximumDistanceToPanel = 0;
    [SerializeField]
    private float _boostSpeed = 0f;
    [SerializeField]
    private float _decayPerFixedUpdate = 0;

    private float _currentAdditionalMomentumMultiplier = 0.0f;
    public void modify(Ball ball)
    {
        if (_currentAdditionalMomentumMultiplier <= 0f)
        {
            if (Input.GetMouseButtonDown(0))
            {
                float distanceToPaddle = Vector2.Distance(transform.position, _paddleTransform.position); //Mouse was pressed and we are close to the paddle.
                if (distanceToPaddle < _maximumDistanceToPanel)
                {
                    ball._currentSpeed = _boostSpeed; //Current speed is now boost speed
                }
            }
        }

        if (ball._currentSpeed > ball._baseSpeed) //Decay the speed until current speed is back to base speed
        {
            if (ball._currentSpeed - _decayPerFixedUpdate < ball._baseSpeed)
                ball._currentSpeed = ball._baseSpeed;
            else
                ball._currentSpeed = ball._currentSpeed - _decayPerFixedUpdate;
        }
    }
}
