﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public enum State
    {
        Ready,
        InMotion,
        Stopped
    }

    [SerializeField] public float _baseSpeed = 2f;
    [SerializeField] private float _bounceAngleAtPaddleEdges = 45f;
    [SerializeField] private CircleCollider2D _collider = null;

    private IBallModifier[] _ballModifiers; //We can have as many modifiers as we like, each one affecting the ball per fixed update.
    public float _currentSpeed = 2f;
    public State state { get; private set; } = State.Ready;
    public Vector2 momentum { get; private set; } = Vector2.zero;
    
    public event Action BallLostEvent = delegate { };

    // Reset the ball to its initial state
    public void Initialize(Transform paddleAnchor)
    {
        _ballModifiers = transform.GetComponents<IBallModifier>();
        transform.SetParent(paddleAnchor);
        transform.localPosition = Vector3.zero;
        momentum = Vector2.zero;
        state = State.Ready;
    }
    
    // Launch the ball from the paddle 
    public void Launch()
    {
        if (state == State.Ready)
        {
            _currentSpeed = _baseSpeed;
            transform.SetParent(null);
            momentum = new Vector2(0f, 1f);
            state = State.InMotion;
        }
    }

    public void stopBall()
    {
        state = State.Stopped;
        momentum = Vector2.zero;
    }

    // FixedUpdate is called once per physics tick
    private void FixedUpdate()
    {
        if (state != State.InMotion)
        {
            return;
        }

        Vector2 newPosition = transform.position;
        float remainingMoveDistance = _currentSpeed * Time.fixedDeltaTime;
        int test = 0;
        while (remainingMoveDistance > 0 && test < 10)
        {
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, _collider.radius, momentum, remainingMoveDistance);
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Hazard"))
                {
                    // Ball lost!
                    state = State.Stopped;
                    momentum = Vector2.zero;
                    
                    Vector2 positionAtCollision = hit.point + _collider.radius * hit.normal;
                    newPosition = positionAtCollision;
                    
                    BallLostEvent();
                }
                else if (hit.collider.CompareTag("Player"))
                {
                    // Bounced off paddle
                    float offsetFromPaddle = hit.transform.position.x - transform.position.x;
                    float angularStrength = offsetFromPaddle / hit.collider.bounds.size.x;
                    float bounceAngle = -_bounceAngleAtPaddleEdges * angularStrength * Mathf.Deg2Rad;
                    momentum = new Vector2(Mathf.Sin(bounceAngle), Mathf.Cos(bounceAngle)).normalized;
                    
                    Vector2 positionAtBounce = hit.point + _collider.radius * hit.normal;
                    remainingMoveDistance -= Vector2.Distance(newPosition, positionAtBounce);
                    newPosition = positionAtBounce;

                    test++;
                }
                else if (hit.collider.CompareTag("Brick"))
                {
                    // Bounced off brick
                    momentum = Vector2.Reflect(momentum, hit.normal);

                    Vector2 positionAtBounce = hit.point + _collider.radius * hit.normal;
                    remainingMoveDistance -= Vector2.Distance(newPosition, positionAtBounce);
                    newPosition = positionAtBounce;

                    test++;

                    hit.collider.gameObject.GetComponent<Brick>().onHit();
                }
                else
                {
                    // Bounced off bounds, etc.
                    momentum = Vector2.Reflect(momentum, hit.normal);
                
                    Vector2 positionAtBounce = hit.point + _collider.radius * hit.normal;
                    remainingMoveDistance -= Vector2.Distance(newPosition, positionAtBounce);
                    newPosition = positionAtBounce;
                }
            }
            else
            {
                newPosition += momentum * remainingMoveDistance;
                break;
            }
        }

        transform.position = newPosition;

        foreach (IBallModifier ballModifier in _ballModifiers) //Apply all the modifiers
        {
            ballModifier.modify(this);
        }
    }
}

interface IBallModifier
{
    void modify(Ball ball);
}
