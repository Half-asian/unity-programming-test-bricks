﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BrickHitHandlerChangeColour : MonoBehaviour, IBrickHitHandler
{
    [SerializeField] private int _health = 2;
    [SerializeField] private Sprite _nextSprite;
    Brick _thisBrick;
    SpriteRenderer _spriteRenderer;
    private void Awake()
    {
        _thisBrick = GetComponent<Brick>();
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    public void onHit()
    {
        _health--;
        _spriteRenderer.sprite = _nextSprite;

        if (_health <= 0)
        {
            LevelEvents.current.onBrickDestroyed(_thisBrick); //Callback when this brick is destroyed
            Destroy(gameObject);
        }
    }
}

