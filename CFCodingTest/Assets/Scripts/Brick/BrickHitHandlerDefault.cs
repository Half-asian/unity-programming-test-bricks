﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
public class BrickHitHandlerDefault : MonoBehaviour, IBrickHitHandler
{
    [SerializeField] private int _health = 1;
    Brick _thisBrick;

    private void Awake()
    {
        _thisBrick = GetComponent<Brick>();
    }

    public void onHit()
    {
        _health--;
        if (_health <= 0)
        {
            LevelEvents.current.onBrickDestroyed(_thisBrick); //Callback when this brick is destroyed
            Destroy(gameObject);
        }
    }
}