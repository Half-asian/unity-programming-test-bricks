﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(IBrickHitHandler))]
public class Brick : MonoBehaviour
{
    //I've used composition rather than inheritance for the bricks. 
    //This way, interfaces can be mixed and matched and reused to create different brick behaviours
    protected IBrickHitHandler _hitHandler; 
    protected IBrickUpdateHandler _updateHandler;

    public string _brickType;

    void Start()
    {
        _hitHandler = GetComponent<IBrickHitHandler>();
        _updateHandler = GetComponent<IBrickUpdateHandler>();
    }

    public void onHit()
    {
        _hitHandler.onHit(); //Attached HitHandler handles the hit.
    }

    private void FixedUpdate()
    {
        if (_updateHandler != null) _updateHandler.update(); //Attached UpdateHandler handles the update.
    }
}

public interface IBrickHitHandler
{
    void onHit();
}

public interface IBrickUpdateHandler
{
    void update();
}

