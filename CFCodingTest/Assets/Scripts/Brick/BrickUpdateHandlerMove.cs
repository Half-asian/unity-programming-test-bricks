﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class BrickUpdateHandlerMove : MonoBehaviour, IBrickUpdateHandler
{
    [SerializeField]
    private float speed;
    public void update()
    {
        transform.Translate(new Vector2(Mathf.Sin(Time.realtimeSinceStartup) * speed, 0)); //Sin function to move brick back and forth
    }
}

